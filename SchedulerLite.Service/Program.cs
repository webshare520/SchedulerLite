﻿using SchedulerLite.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace SchedulerLite.Service
{
    /*
     1加载时间轮任务
     2加载定时执行的任务
    */
    class Program
    {
        static TaskTimer taskTimer;

        static void Main(string[] args)
        {
            Console.Title = "任务调度服务-SchedulerLite";
            taskTimer = new TaskTimer(new DelayTaskManager(Config.TimeWheelSlotNum), new TimedTaskManager());
            taskTimer.Start();
            while (true)
            {
                var exit = Console.ReadLine();
                if (exit == "exit")
                {
                    Console.WriteLine("退出");
                    break;
                }
            }
        }
    }
}
