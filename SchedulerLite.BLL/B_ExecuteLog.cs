﻿using Loogn.OrmLite;
using SchedulerLite.Common;
using SchedulerLite.DAL;
using SchedulerLite.Model.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchedulerLite.BLL
{
    public class B_ExecuteLog
    {
        public static long Add(ExecuteLog m)
        {
            m.AddTime = DateTime.Now;
            Console.WriteLine("TaskId:{0},Type:{1},Name:{2},Method:{3},Url:{4},Data:{5},Status:{6},Message:{7},Time:{8}",
                m.TaskId, m.TaskType == 1 ? "定时" : "延迟", m.TaskName, m.TaskMethod, m.TaskUrl, m.PostData, m.GetStatus(), m.Message, m.AddTime.ToString("yyyy-MM-dd HH:mm:ss"));
            if (m.Status != 1 || Config.RecordSuccessLog)
            {
                return D_ExecuteLog.Add(m);
            }
            else
            {
                return 0;
            }
        }

        public static OrmLitePageResult<ExecuteLog> SearchList(long taskId, int taskType, int status, int pageIndex, int pageSize)
        {
            return D_ExecuteLog.SearchList(taskId, taskType, status, pageIndex, pageSize);
        }
    }
}
